Offwhite
====

This is a quick and dirty prototype of the front page of [whitepages.com.au](whitepages.com.au). All of the front end code you're interested in is in app/index.html. The nodejs server is there only as a proxy for the whitepages.com.au api.

To get started, run:

    npm install    # first run only
    npm start

And browse to [http://localhost:3000/](http://localhost:3000/)

Enjoy! And if you do have an update, push!

-Tushar
