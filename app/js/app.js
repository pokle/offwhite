var app = angular.module('app',[]);

// Tab values that are shared across controllers
app.value('tabs', {
	tabs: ["Business", "Government", "Residential"],
	active: "Business"
})
