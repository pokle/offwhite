app.filter('location', 
    function() {
        return function(searchSummary) {
            if (searchSummary.location == "EMPTY LOCALITY") {
                return searchSummary.state
            } else {
              return searchSummary.location + ", " + searchSummary.state;
            }
        }
    }
);
