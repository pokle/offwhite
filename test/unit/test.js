'use strict';

/* jasmine specs for services go here */

describe('the test', function() {
  describe('version', function() {
    it('should return current version', function() {
      expect(9.5).toEqual(9.5);
    });
  });
});
