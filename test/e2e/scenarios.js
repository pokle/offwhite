'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('offwhite', function() {

  beforeEach(function() {
    browser().navigateTo('/');
  });
  
  it('should find no results', function() {
	  expect(element('#summary').text()).
	  	toMatch(/No +results +found +/);
  });
  

});
