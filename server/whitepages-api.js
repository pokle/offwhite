
/*
 * /search 
 *
 * Proxy for wpol dynamic search
 */

var http = require('http'),
    url = require('url');

var wpol_url_format = { 
  protocol: 'http:',
  host: 'www.whitepages.com.au',
  pathname: '/dynamic-search/dynamicSearch.action' 
}

function routeThroughProxyIfRequired(uri) {
  if (process.env.http_proxy) {
    var proxy = url.parse(process.env.http_proxy)
    return {
      path: uri, 
      host: proxy.hostname, 
      port: proxy.port,
      headers: {
        Host: wpol_url_format.host
      }
    }
  } else {
    return uri
  }
}

exports.search = function(req, res){

    var query = url.parse(req.url).query;

    wpol_url_format.search = query; // Just pass through our own query!!!
    var wpol_url = url.format(wpol_url_format)

    function error(e) {
        console.error('Got error', e)
        res.send({error: e.message})
    }

    http.get(routeThroughProxyIfRequired(wpol_url), function(wpolResponse) {
      console.log('Got response', wpolResponse.statusCode)
      res.setHeader("Content-Type", "application/json");
      res.setHeader("Expires", new Date(Date.now() + 1000 * 60 * 60 * 24).toGMTString())
      wpolResponse.pipe(res)
    }).on('error', error);
};