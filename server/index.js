var express = require('express')
  , api = require('./whitepages-api')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, '../app')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/s*', function(req, res) {
  res.sendfile(path.join(__dirname, '../app/index.html'))
});

app.get('/api', api.search);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
